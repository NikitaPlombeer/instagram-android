package com.plombeer.instagram;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.plombeer.instagram.adapters.FeedRecycleViewAdapter;
import com.plombeer.instagram.api.API;
import com.plombeer.instagram.api.Result;
import com.plombeer.instagram.entity.FeedCard;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FeedActivity extends AppCompatActivity {

    private FeedRecycleViewAdapter mAdapter;

    @Bind(R.id.lvMain)
    protected RecyclerView lvMain;

//    @Bind(R.id.app_bar)
//    protected Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);

        lvMain.setHasFixedSize(true);
        new FeedAsynsTask().execute(0);
    }


    public class FeedAsynsTask extends AsyncTask<Integer, Void, ArrayList<FeedCard>> {

        @Override
        protected ArrayList<FeedCard> doInBackground(Integer... params) {
            Result feed = API.getFeed(params[0]);
            JSONArray arr = feed.getJSONArray();

            ArrayList<FeedCard> cards = new ArrayList<>();
            try {
                for (int i = 0; i < arr.length(); i++) {
                    cards.add(new FeedCard(arr.getJSONObject(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return cards;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedCard> cards) {
            mAdapter = new FeedRecycleViewAdapter(cards);
            lvMain.setAdapter(mAdapter);
            lvMain.setLayoutManager(new LinearLayoutManager(FeedActivity.this));
        }
    }
}
