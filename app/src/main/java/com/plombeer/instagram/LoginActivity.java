package com.plombeer.instagram;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.plombeer.instagram.api.API;
import com.plombeer.instagram.api.Const;
import com.plombeer.instagram.api.FillType;
import com.plombeer.instagram.api.Result;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {


    @Bind(R.id.usernameEditText)
    protected EditText usernameEditText;

    @Bind(R.id.passwordEditText)
    protected EditText passwordEditText;

    @Bind(R.id.infoTextView)
    protected TextView infoTextView;

    @Bind(R.id.progress_wheel)
    protected ProgressWheel progressWheel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        tryToLogin();

    }

    public void onLoginButtonClick(View v){
        if(API.isOnline()) {
            new LoginAsyncTask().execute(usernameEditText.getText().toString(), passwordEditText.getText().toString());
        } else{
            infoTextView.setText(R.string.no_interner_connection);
        }
    }

    public void onRegisterButtonClick(View v){
        this.overridePendingTransition(R.anim.animation_enter,
                R.anim.animation_leave);
        startActivity(new Intent(this, RegisterActivity.class));
    }

    private void tryToLogin(){
        if(API.isOnline()) {
            boolean isLogin = Const.mSharedPreferences.getBoolean(Const.PREF_IS_LOGIN, false);
            if (isLogin) {
                String username = Const.mSharedPreferences.getString(Const.PREF_USERNAME, "");
                String password = Const.mSharedPreferences.getString(Const.PREF_PASSWORD, "");
                new LoginAsyncTask().execute(username, password);
            }
        }
    }

    public class LoginAsyncTask extends AsyncTask<String, Void, Result>{

        @Override
        protected void onPreExecute() {
            progressWheel.setVisibility(View.VISIBLE);
        }

        private String username;
        private String password;

        @Override
        protected Result doInBackground(String... params) {
            username = params[0];
            password = params[1];
            return new Result(API.login(username, password));
        }

        @Override
        protected void onPostExecute(Result result) {
            if(result.code() == 200) {
                String ACCESS_TOKEN = result.header(FillType.ACCESS_TOKEN);
                Const.mSharedPreferences.edit()
                        .putString(FillType.ACCESS_TOKEN, ACCESS_TOKEN)
                        .putBoolean(Const.PREF_IS_LOGIN, true)
                        .putString(Const.PREF_USERNAME, username)
                        .putString(Const.PREF_PASSWORD, password)
                        .apply();
                API.setAccessToken(ACCESS_TOKEN);
                infoTextView.setText(result.getJSONObject().optString(FillType.message));

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            } else{
                JSONObject resultJSONObject = result.getJSONObject();
                infoTextView.setText(resultJSONObject.optString(FillType.error));
            }
            result.print();
            progressWheel.setVisibility(View.INVISIBLE);
        }
    }
}
