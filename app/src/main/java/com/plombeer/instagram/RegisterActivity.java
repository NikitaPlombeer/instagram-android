package com.plombeer.instagram;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.plombeer.instagram.api.API;
import com.plombeer.instagram.api.Const;
import com.plombeer.instagram.api.FillType;
import com.plombeer.instagram.api.Result;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.emailEditText)
    protected EditText emailEditText;

    @Bind(R.id.usernameEditText)
    protected EditText usernameEditText;

    @Bind(R.id.passwordEditText)
    protected EditText passwordEditText;

    @Bind(R.id.repeatPasswordEditText)
    protected EditText repeatPasswordEditText;

    @Bind(R.id.infoTextView)
    protected TextView infoTextView;

    @Bind(R.id.progress_wheel)
    protected ProgressWheel progressWheel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    public void onRegisterButtonClick(View v){
        String username = usernameEditText.getText().toString().trim();
        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString();
        String repeatPassword = repeatPasswordEditText.getText().toString();

        if(username.equals("")) {
            infoTextView.setText(getString(R.string.field_cannot_be_empty).replaceAll("%s", "username"));
            return;
        }

        if(email.equals("")){
            infoTextView.setText(getString(R.string.field_cannot_be_empty).replaceAll("%s", "email"));
            return;
        }

        if(password.length() < 6){
            infoTextView.setText(R.string.small_password);
            return;
        }

        if(!password.equals(repeatPassword)){
            infoTextView.setText(R.string.password_not_equals);
            return;
        }

        new RegisterAsyncTask().execute(username, email, password);
    }

    public class RegisterAsyncTask extends AsyncTask<String, Void, Result> {

        @Override
        protected void onPreExecute() {
            progressWheel.setVisibility(View.VISIBLE);
        }

        @Override
        protected Result doInBackground(String... params) {
            return new Result(API.register(params[0], params[1], params[2]));
        }

        @Override
        protected void onPostExecute(Result result) {
            if(result.code() == 200) {
                infoTextView.setText(R.string.success_sign_up);
            } else{
                infoTextView.setText(result.getJSONObject().optString(FillType.message));
            }
            result.print();
            progressWheel.setVisibility(View.INVISIBLE);
        }
    }
}
