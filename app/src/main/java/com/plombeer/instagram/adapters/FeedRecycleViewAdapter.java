package com.plombeer.instagram.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.plombeer.instagram.R;
import com.plombeer.instagram.api.API;
import com.plombeer.instagram.entity.FeedCard;
import com.plombeer.instagram.views.SquareImageView;

import java.util.ArrayList;

/**
 * Created by Никита on 15.07.2016.
 */
public class FeedRecycleViewAdapter extends RecyclerView.Adapter<FeedRecycleViewAdapter.ViewHolder>{


    DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.empty) // resource or drawable
            .showImageForEmptyUri(R.drawable.empty) // resource or drawable
            .showImageOnFail(R.drawable.empty) // resource or drawable
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

    DisplayImageOptions avatarOptions = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.empty) // resource or drawable
            .showImageForEmptyUri(R.drawable.empty) // resource or drawable
            .showImageOnFail(R.drawable.empty) // resource or drawable
            .displayer(new CircleBitmapDisplayer())
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

    private ArrayList<FeedCard> cards;

    public FeedRecycleViewAdapter(ArrayList<FeedCard> cards) {
        this.cards = cards;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_card_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        FeedCard card = cards.get(position);
        holder.textView.setText(card.getUsername() + ": " + card.getText());
        holder.usernameTextView.setText(card.getUsername());
        holder.likeTextView.setText(card.getLikes()+"");
        ImageLoader.getInstance().displayImage(API.getAvatarUrlByUsername(card.getUsername()), holder.avatarImageView, avatarOptions);

        ImageLoader.getInstance().displayImage(API.getPhotoUrl(card.getImageUrl()), holder.imageView, options);
        holder.likeImageView.setImageResource(R.drawable.ic_like_default);
        holder.likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.likeImageView.setImageResource(R.drawable.ic_likered);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        SquareImageView imageView;
        TextView usernameTextView;
        ImageView avatarImageView;
        ImageView likeImageView;
        TextView textView;
        TextView likeTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            avatarImageView = (ImageView) itemView.findViewById(R.id.avatarImageView);
            imageView = (SquareImageView) itemView.findViewById(R.id.imageView);
            likeImageView = (ImageView) itemView.findViewById(R.id.likeImageView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            usernameTextView = (TextView) itemView.findViewById(R.id.usernameTextView);
            likeTextView = (TextView) itemView.findViewById(R.id.likeTextView);
        }
    }
}
