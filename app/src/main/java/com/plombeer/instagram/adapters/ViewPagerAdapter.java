package com.plombeer.instagram.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plombeer.instagram.fragments.FeedFragment;
import com.plombeer.instagram.fragments.PhotoFragment;

/**
 * Created by plombeer on 16.07.16.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter{

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0)
            return FeedFragment.newInstance();
        if(position == 1)
            return PhotoFragment.newInstance();
        return new Fragment();
    }

    @Override
    public int getCount() {
        return 4;
    }
}
