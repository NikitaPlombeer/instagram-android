package com.plombeer.instagram.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Никита on 14.07.2016.
 */
public class API {
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = "API";
    private static String accessToken = Const.mSharedPreferences.getString(FillType.ACCESS_TOKEN, "");

    private static synchronized Response post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .post(body)
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    private static synchronized Response patch(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .patch(body)
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    private static synchronized Response put(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .put(body)
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    private static Response get(String url) throws IOException {
        Log.i(TAG, url);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .get()
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    private static synchronized Response delete(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .delete()
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    private static synchronized Response head(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader(FillType.ACCESS_TOKEN, accessToken)
                .head()
                .build();
        return getOkHttpClient().newCall(request).execute();
    }

    public static void setAccessToken(String xAuthToken) {
        accessToken = xAuthToken;
    }

    private static OkHttpClient okHttpClient;

    private static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            Log.i(TAG, "OkHttpClient create");

            SSLContext sslContext;
            try {
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, null, null);
            } catch (Exception e) {
                throw new AssertionError(); // The system has no TLS. Just give up.
            }

            okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(90, TimeUnit.SECONDS)
                    .readTimeout(90, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .sslSocketFactory(sslContext.getSocketFactory())
                    .build();
        }
        return okHttpClient;
    }


    public static Response login(String login, String password) {
        JSONObject object = new JSONObject();
        try {
            object.put(FillType.username, login);
            object.put(FillType.password, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String url = Const.host + "/login";
            String json = object.toString();
            Log.i("API::login", "POST " + url + " " + json);
            return post(url, json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void logout() {
        Const.mSharedPreferences.edit()
            .putString(FillType.ACCESS_TOKEN, "")
            .putBoolean(Const.PREF_IS_LOGIN, false)
            .putString(Const.PREF_USERNAME, "")
            .putString(Const.PREF_PASSWORD, "")
            .apply();
    }

    public static Result getProfile() {
        try {
            String url = Const.host + "/api/me";
            Log.i("API::login", "GET " + url);
            return new Result(get(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Result getFeed(int page){
        String url = Const.host + "/api/feed/"+page;
        Response response = null;
        try {
            response = get(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Result(response);
    }

    public static synchronized Response uploadImageWithProgress(byte[] byteArray,CountingFileRequestBody.ProgressListener listener){
        try {

            String url = Const.host + "/api/files";
            Log.d(TAG, url);
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("text", "fuck u")
                    .addFormDataPart("file", "filename",  new CountingFileRequestBody(byteArray, listener))
                    .build();
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .addHeader(FillType.ACCESS_TOKEN, accessToken)
                    .build();
            return getOkHttpClient().newCall(request).execute();
        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e(TAG, "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e(TAG, "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) MyApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static Response register(String username, String email, String password){
        JSONObject object = new JSONObject();
        try {
            object.put(FillType.username, username);
            object.put(FillType.email, email);
            object.put(FillType.password, password);

            String json = object.toString();
            String url = Const.host + "/register";
            Log.i("API::register", "POST " + url + " " + json);

            return post(url, json);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPhotoUrl(String documentId){
        return Const.host + "/document/"+documentId;
    }

    public static String getAvatarUrlByUsername(String username){
        return Const.host + "/document/avatar/"+username;
    }
}
