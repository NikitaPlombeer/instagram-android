package com.plombeer.instagram.api;

import android.content.SharedPreferences;

public class Const {

//    static {
//        SVG svg = new SVGBuilder()
//                .readFromResource(MyApp.getContext().getResources(), R.raw.like) // if svg in res/raw
//                // .setWhiteMode(true) // draw fills in white, doesn't draw strokes
//                // .setColorSwap(0xFF008800, 0xFF33AAFF) // swap a single colour
//                // .setColorFilter(filter) // run through a colour filter
//                // .set[Stroke|Fill]ColorFilter(filter) // apply a colour filter to only the stroke or fill
//                .build();
//        likeDrawable = svg.getDrawable();
//    }
//
//    public static Drawable likeDrawable;
    public static final String host = "http://192.168.1.2:3041";
    public static SharedPreferences mSharedPreferences;
    public static final String PREFERENCES_NAME = "InstPreferences";

    public static final String PREF_USERNAME = "username";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_IS_LOGIN = "isLogin";
}