package com.plombeer.instagram.api;

import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * Created by Никита on 25.02.2016.
 */
public class CountingFileRequestBody extends RequestBody {

    private static final int SEGMENT_SIZE = 2048; // okio.Segment.SIZE

    private final byte[] byteArray;
    private final ProgressListener listener;

    public CountingFileRequestBody(byte[] byteArray, ProgressListener listener) {
        this.byteArray = byteArray;
        this.listener = listener;
    }

    @Override
    public long contentLength() {
        return byteArray.length;
    }


    @Override
    public MediaType contentType() {
        return MediaType.parse("image/jpeg");
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {
        Source source = null;
        try {


            source = Okio.source(new ByteArrayInputStream(byteArray));
            long total = 0;
            long read;

            while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                total += read;
                sink.flush();
                Log.i("progress", String.valueOf(total));
                if(this.listener != null)
                    this.listener.transferred((int)((float)total / (float)contentLength() * 100f));

            }
            if(this.listener != null)
                this.listener.transferred(100);
        } catch (Exception e) {
            e.printStackTrace();
        }  finally {
            Util.closeQuietly(source);
        }
    }

    public interface ProgressListener {
        void transferred(int num);
    }

}