package com.plombeer.instagram.api;

public class FillType {

    public static final String username = "username";
    public static final String password = "password";
    public static final String id = "id";
    public static final String name = "name";
    public static final String email = "email";
    public static final String _createdOn = "_createdOn";
    public static final String createdOn = "createdOn";
    public static final String ACCESS_TOKEN = "ACCESS-TOKEN";
    public static final String message = "message";
    public static final String error = "error";
    public static final String avatar = "avatar";
    public static final String imageUrl = "imageUrl";
    public static final String text = "text";
    public static final String likes = "likes";
}