package com.plombeer.instagram.api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Response;

/**
 * Created by Никита on 14.07.2016.
 */
public class Result {

    private boolean isNull;
    private String json;
    private int code;
    private Headers headers;

    public Result(Response res){
        if(res == null){
            isNull = true;
            return;
        }

        isNull = false;
        code = res.code();
        headers = res.headers();
        try {
            json = res.body().string();
            res.body().close();
        } catch (IOException e) {
            isNull = true;
            headers = null;
            code = -1;
            e.printStackTrace();
        }

    }

    public boolean isNull() {
        return isNull;
    }

    public String json(){
        return json;
    }

    public int code() {
        return code;
    }

    public JSONObject getJSONObject(){
        try {
            return new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONArray getJSONArray(){
        try {
            return new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void print(){
        Log.i("Result", code + " "+json);
    }


    public String header(String key) {
        return headers.get(key);
    }
}
