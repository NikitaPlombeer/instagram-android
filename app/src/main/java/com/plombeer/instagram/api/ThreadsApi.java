package com.plombeer.instagram.api;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Никита on 30.11.2015.
 */
public class ThreadsApi {

    private final static int THREADS_LIMIT = 10;

    private static ThreadPoolExecutor threadExecutor = null;

    private static ThreadPoolExecutor getService() {
        if (threadExecutor == null) {
            threadExecutor = new ThreadPoolExecutor(THREADS_LIMIT, THREADS_LIMIT,
                    0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>());
        }

        return threadExecutor;
    }

    public static void run(Runnable thread) {
        getService().execute(thread);
    }
}