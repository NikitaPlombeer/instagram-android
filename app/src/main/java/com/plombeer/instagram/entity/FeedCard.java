package com.plombeer.instagram.entity;

import com.plombeer.instagram.api.FillType;

import org.json.JSONObject;

/**
 * Created by Никита on 15.07.2016.
 */
public class FeedCard {

    private String username;
    private String userAvatarUrl;
    private String imageUrl;
    private String text;
    private int likes;
    private long createdOn;

    public FeedCard(JSONObject json){
        username = json.optString(FillType.username);
        userAvatarUrl = json.optString(FillType.avatar);
        imageUrl = json.optString(FillType.imageUrl);
        text = json.optString(FillType.text);
        likes = json.optInt(FillType.likes);
        createdOn = json.optLong(FillType.createdOn);
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public String getUsername() {
        return username;
    }

    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getText() {
        return text;
    }

    public int getLikes() {
        return likes;
    }
}
