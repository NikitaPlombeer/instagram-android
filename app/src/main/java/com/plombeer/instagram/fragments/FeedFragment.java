package com.plombeer.instagram.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.plombeer.instagram.R;
import com.plombeer.instagram.adapters.FeedRecycleViewAdapter;
import com.plombeer.instagram.api.API;
import com.plombeer.instagram.api.MyApp;
import com.plombeer.instagram.api.Result;
import com.plombeer.instagram.entity.FeedCard;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.Bind;

/**
 * Created by plombeer on 16.07.16.
 */
public class FeedFragment extends Fragment{

    private static final String TAG = FeedFragment.class.getSimpleName();

    public static FeedFragment newInstance() {
        Bundle bundle = new Bundle();

        FeedFragment feedFragment = new FeedFragment();
        feedFragment.setArguments(bundle);

        return feedFragment;
    }
    public FeedFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
    }

    private FeedRecycleViewAdapter mAdapter;
    protected RecyclerView lvMain;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its view hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the view hierarchy; it would just never be used.
            return null;
        }

        View view = inflater.inflate(R.layout.activity_feed, null);
        lvMain = (RecyclerView) view.findViewById(R.id.lvMain);
        lvMain.setHasFixedSize(true);
        new FeedAsynsTask().execute(0);

        return view;
    }

    public class FeedAsynsTask extends AsyncTask<Integer, Void, ArrayList<FeedCard>> {

        @Override
        protected ArrayList<FeedCard> doInBackground(Integer... params) {
            Result feed = API.getFeed(params[0]);
            JSONArray arr = feed.getJSONArray();

            ArrayList<FeedCard> cards = new ArrayList<>();
            try {
                for (int i = 0; i < arr.length(); i++) {
                    cards.add(new FeedCard(arr.getJSONObject(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return cards;
        }

        @Override
        protected void onPostExecute(ArrayList<FeedCard> cards) {
            mAdapter = new FeedRecycleViewAdapter(cards);
            lvMain.setAdapter(mAdapter);
            lvMain.setLayoutManager(new LinearLayoutManager(MyApp.getContext()));
        }
    }
}
