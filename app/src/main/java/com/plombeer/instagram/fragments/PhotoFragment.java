package com.plombeer.instagram.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.plombeer.instagram.R;
import com.plombeer.instagram.adapters.FeedRecycleViewAdapter;
import com.plombeer.instagram.api.API;
import com.plombeer.instagram.api.CountingFileRequestBody;
import com.plombeer.instagram.api.MyApp;
import com.plombeer.instagram.api.Result;
import com.plombeer.instagram.api.ThreadsApi;
import com.plombeer.instagram.entity.FeedCard;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.TimerTask;

/**
 * Created by plombeer on 16.07.16.
 */
public class PhotoFragment extends Fragment{

    private static final String TAG = PhotoFragment.class.getSimpleName();

    public static PhotoFragment newInstance() {
        Bundle bundle = new Bundle();

        PhotoFragment feedFragment = new PhotoFragment();
        feedFragment.setArguments(bundle);

        return feedFragment;
    }
    public PhotoFragment() {
    }

    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
    }

    private static final int SELECT_PHOTO = 43;
    private static final int REQUEST_CODE = 2;
    private static final String[] perms = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                boolean choose = true;
                for(int i = 0; i < permissions.length; i++) {
                    int result = grantResults[i];
                    if(result == -1)
                        choose = false;
                    String permission = permissions[i];
                    Log.i("PERMISSION", result + ": " + permission);
                }
                if(choose)
                    choosePhoto();
                break;
            }
        }
    }

    public void choosePhoto(){
        Intent pickIntent = new Intent();
        pickIntent.setType("image/*");
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

//            String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
        Intent chooserIntent = Intent.createChooser(pickIntent, "");
        chooserIntent.putExtra
                (
                        Intent.EXTRA_INITIAL_INTENTS,
                        new Intent[]{takePhotoIntent}
                );

        startActivityForResult(chooserIntent, SELECT_PHOTO);
    }

    public void openDialog() {
        try {
            ActivityCompat.requestPermissions(getActivity(), perms, REQUEST_CODE);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            ThreadsApi.run(new TimerTask() {
                @Override
                public void run() {
                    onPhotoUpload(data);
                }
            });
        }
    }

    public synchronized void onPhotoUpload(Intent data){
        Bitmap bitmap;
        if(data.hasExtra("data")) {
            bitmap = (Bitmap) data.getExtras().get("data");
        } else {
            InputStream imageStream = null;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(data.getData());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if(imageStream == null) return;
            bitmap = BitmapFactory.decodeStream(imageStream);
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        final byte[] byteArray = stream.toByteArray();


        ThreadsApi.run(new Runnable() {
            @Override
            public void run() {
                API.uploadImageWithProgress(byteArray, new CountingFileRequestBody.ProgressListener() {
                    @Override
                    public void transferred(final int num) {

                        new Handler(MyApp.getContext().getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                System.out.println(num);
                                if(num == 100)
                                    progressBar.setProgress(0);
                                else
                                    progressBar.setProgress(num);
                            }
                        });
                    }
                });
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (container == null) return null;

        View view = inflater.inflate(R.layout.activity_test_photo_upload, null);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        Button button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        return view;
    }

}
